# Vehicle Counting OpenCV
This project aims to deploy on raspberry pi with an camera to count how many vehicle pass through an road.

The project is based on the an example to track object in an video. The idea is to futher extend the ability tell how many cars are passing through.

The original project use backgroun substruction algorithm to remove the background and locate the object.

What need to be further develop:
1. Take an video for development
2. Apply bg substraction algorithm to remove background and locate all moving objects
3. Removing noise(small object identified)


Todo:
4. If we got 10 objects in a single frame, establish an relation with the objects located in the next frame.Establish the relationship between objects.
5. Handle the object jump in and out of the screen. It will affect the counts.
6. draw a line, when a object cross it add 1. But do not add again, as the it will take multiple frame for same object ot pass through.
7. Instead of using the video stored. Use real time image from raspberry pi.


Status:
This project has been given up. The count between frames is not stable enough. The variation is too large to handle. Shadow is also causing problems in counting.

Possible solution to fix the problem:
1. Apply object tracking algorithm to track the object.
2. To apply object tracking algorithm, a inital coordinate need to be provided. The corrdinate could be provided by object recognition algorithm.
3. OpenCV has both algorithm. However, it will need a lot of images to train the recognition model.


#Achievement
See vehicle-count.png in this repo.

#Get start
python3 bgsubtract2.py

