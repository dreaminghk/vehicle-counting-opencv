import numpy as np
import cv2
import imutils


#config
SMALLEST_CAR_SIZE = 80
DILATE_KERNEL_SIZE = 3
PROCESS_IN_GREY = True


cap = cv2.VideoCapture('video/IMG_9578.MOV')


#use MOG2
fgbg = cv2.createBackgroundSubtractorMOG2(detectShadows = False)

#use GMG algorithm instead
#kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3))
#fgbg = cv2.bgsegm.createBackgroundSubtractorGMG()

while(1):
	ret, frame = cap.read()

	frame = imutils.resize(frame, width=500)
	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	gray = cv2.GaussianBlur(gray if PROCESS_IN_GREY else frame, (21, 21), 0)

	cv2.imshow('gray',gray)

	fgmask = fgbg.apply(gray)


	kernel = np.ones((DILATE_KERNEL_SIZE,DILATE_KERNEL_SIZE),np.uint8)
	#erosion = cv2.erode(fgmask,kernel,iterations = 1)

	fgmask = cv2.dilate(fgmask,kernel,iterations = 1)

	#imgray = cv2.cvtColor(im,cv2.COLOR_BGR2GRAY)
	ret,thresh = cv2.threshold(fgmask,127,255,0)

	#remove noise
	kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3))
	fgmask = cv2.morphologyEx(fgmask, cv2.MORPH_OPEN, kernel)
	contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)    

	#image = cv2.drawContours(frame.copy(), contours, -1, (0,255,0), 3)

	# loop over the contours

	result = frame.copy()

	countOfCar = 0

	for c in contours:
		# if the contour is too small, ignore it
		if cv2.contourArea(c) < SMALLEST_CAR_SIZE:
			continue

		# compute the bounding box for the contour, draw it on the frame,
		# and update the text
		(x, y, w, h) = cv2.boundingRect(c)
		cv2.rectangle(result, (x, y), (x + w, y + h), (0, 255, 0), 2)
		countOfCar+=1
		
	# draw the text and timestamp on the frame
	cv2.putText(result, "No. of Car: {}".format(countOfCar), (10, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)


	cv2.imshow('original',frame)

	cv2.imshow('fgmask',fgmask)

	cv2.imshow('final',result)



	k = cv2.waitKey(30) & 0xff
	if k == 27:
		break



cap.release()
cv2.destroyAllWindows()